// gcc -Wall -O2 -pthread tpool.c -o tpool
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "sorter.h"

#define N 1000000 // Arry size
#define THREADS 4 // How many threads in the thread pool
#define QUEUE_SIZE 1000000 // Size of the queue

// Struct of information passed to each thread
struct message {
    int n; // How many elements to process
    int complete; // Did the thread completed a partial sorting?
    int shutdown; // Does the thread need to shutdown?
    double *start; // Where to begin from
};

int head = 0; // Index of the head of the queue
int tail = 0; // Index of the tail of the queue
// Counts how many messages are in the queue, waiting to be processed
int global_availmsg = 0;
struct message queue[QUEUE_SIZE]; // The queue

// Conditional variable for message insertion
pthread_cond_t msg_in = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex

// The thread work function
void *thread_func(void *args) {
    int pivot;
    struct message tmessage;

    while(1) {
        pthread_mutex_lock(&mutex); // Lock mutex
        while(global_availmsg < 1) {
            // Wait until there is an available message in the queue
            pthread_cond_wait(&msg_in, &mutex);
        }
        tmessage = queue[tail]; // Local variable

        // If the message picked up has its shutdown flag set to 1 terminate
        if(tmessage.shutdown == 1) {
            break;
        }
        // If the message picked up has its complete
        // flag set to 1 push it back to the queue
        if(tmessage.complete == 1) {
            queue[head].complete = 1;
            head = (head + 1) % QUEUE_SIZE;
            global_availmsg += 1;
        }
        else {
            // Sort
            // If <=5000 elements to process apply quicksort yourself
            if(tmessage.n <= 5000) {
                quicksort(tmessage.start, tmessage.n);
                queue[head].n = tmessage.n;
                queue[head].complete = 1;
                head = (head + 1) % QUEUE_SIZE;
                global_availmsg += 1;
            }
            else { // If >5000 elements to process: divide and conquer
                // Set pivot
                pivot = partition(tmessage.start, tmessage.n);

                // Message for elements left of pivot
                queue[head].start = tmessage.start;
                queue[head].n = pivot;
                head = (head + 1) % QUEUE_SIZE;
                global_availmsg += 1;

                // Message for elements right of pivot
                queue[head].start = tmessage.start + pivot;
                queue[head].n = tmessage.n - pivot;
                head = (head + 1) % QUEUE_SIZE;
                global_availmsg += 1;
            }
        }
        // Inform queue that you handled this message
        tail = (tail + 1) % QUEUE_SIZE;
        global_availmsg -= 1;
        pthread_mutex_unlock(&mutex); // Unlock mutex
    }

    pthread_mutex_unlock(&mutex); // Unlock mutex
    pthread_exit(NULL);// exit thread
}

int main() {
    int i, sorted;
    double *a;
    pthread_t thread[THREADS];

    // Allocating space for the array
    if((a = (double *)malloc(N * sizeof(double))) == NULL) {
        printf("Error at allocating a\n");
        exit(1);
    }

    // Initialising array with random values
    srand(time(NULL));
    for(i = 0; i < N; i++) {
        a[i] = (double)rand() / RAND_MAX;
    }

    //Initialising queue
    for(i = 0; i < QUEUE_SIZE; i++) {
        queue[i].n = 0;
        queue[i].start = a;
        queue[i].complete = 0;
        queue[i].shutdown = 0;
    }

    // Creating 2 threads
    for(i = 0; i < THREADS; i++) {
        pthread_create(&thread[i], NULL, thread_func, NULL);
    }

    // Insert the first packet into the queue
    queue[head].n = N;
    queue[head].start = a;
    queue[head].complete = 0;
    queue[head].shutdown = 0;
    head = (head + 1) % QUEUE_SIZE;
    global_availmsg += 1;
    pthread_cond_signal(&msg_in);

    // Check the queue for complete messages until
    // all elements of the array are sorted
    sorted = 0;
    while(sorted < N) {
        for(i = 0; i < N; i++) {
            pthread_mutex_lock(&mutex);
            if(queue[i].complete == 1) {
                sorted += queue[i].n;
            }
            pthread_mutex_unlock(&mutex);
        }
    }

    // Insert the shutdown message into the queue
    queue[head].shutdown = 1;
    head = (head + 1) % QUEUE_SIZE;
    global_availmsg += 1;
    pthread_cond_signal(&msg_in);

    // Joing threads
    for(i = 0; i < THREADS; i++) {
        pthread_join(thread[i], NULL);
    }

    // Check
    for(i = 0; i < (N -1); i++) {
        if(a[i] > a[i + 1]) {
            printf("Error at element %d\n", i);
            break;
        }
    }

    free(a);
    pthread_cond_destroy(&msg_in);
    pthread_mutex_destroy(&mutex);

    return 0;
}
